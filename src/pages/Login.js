import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
// User Context
import UserContext from '../UserContext';

export default function Login() {
	// Allows us to consume the UserContext object and its'sproperties to be used for user validation
	const { user, setUser } = useContext(UserContext);
	console.log(user)
	console.log(setUser)

	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');

	const [ isActive, setIsActive] = useState(false);
	useEffect(()=>{
		if (email !== ''&& password !== '') {
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[email, password]);

	function loginUser(e){
		e.preventDefault();
		/*
			localStorage.setItem allows us to manipulate the browser's local storage to store information indefinitely to help demonstrate the conditional rendering
			localStorage does not trigger rerendering of components; so for it to be able to take effect, we have to refresh the browser
		*/
		localStorage.setItem('email', email);

		/*
			using the setUser function, change the value of the user variable to the email coming from the localStorage. the argument that the setUser should get is a form of an object.

			6:11 pm
		*/
		setUser({
			email: localStorage.getItem('email')
		});

		setEmail('');
		setPassword('');

		alert('User successfully logged in')
	}


	return(
		(user.email !== null)?
		<Navigate to='/courses' />
		:
		  <Form onSubmit={(e) => loginUser(e)}>
		    <Form.Group controlId="userEmail">
		      <Form.Label>Email address</Form.Label>
		      <Form.Control 
		      				type="email" 
		      				placeholder="Enter email" 
		      				value={email}
		      				onChange={e => setEmail(e.target.value)}
		      				required 
				/>
		      <Form.Text className="text-muted">
		        We'll never share your email with anyone else.
		      </Form.Text>
		    </Form.Group>

		    <Form.Group controlId="password1">
		      <Form.Label>Password</Form.Label>
		      <Form.Control
		      				type="password" 
		      				placeholder="Password" 
		      				value={password}
		      				onChange={e => setPassword(e.target.value)}
		      				required
				/>
		    </Form.Group>

		    {isActive ?
		    <Button variant="primary" type="submit" id="submitBtn">Login</Button>
		    :
		    <Button variant="primary" type="submit" id="submitBtn" disabled>Login</Button>
			}
		  </Form>
		);
}